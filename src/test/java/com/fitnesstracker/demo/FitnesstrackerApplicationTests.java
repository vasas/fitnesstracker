package com.fitnesstracker.demo;

import static com.fitnesstracker.model.Unit.PORTION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import com.fitnesstracker.model.Food;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FitnesstrackerApplicationTests {

  @Test
  public void contextLoads() {
    Food food = new Food(1, "food1", "description", PORTION);
    assertNotNull(food);
    assertEquals("food1", food.getName());
    assertEquals("description", food.getDescription());
    assertSame(PORTION, food.getUnit());
  }
}
