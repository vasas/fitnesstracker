package com.fitnesstracker.model;

public class Food {

  private int id;

  private String name;

  private String description;

  private Unit unit;

  public Food() {
    super();
  }

  public Food(int id, String name, String description, Unit unit) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.unit = unit;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Unit getUnit() {
    return unit;
  }

  public void setUnit(Unit unit) {
    this.unit = unit;
  }
}
