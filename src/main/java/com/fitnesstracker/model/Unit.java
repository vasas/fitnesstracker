package com.fitnesstracker.model;

public enum Unit {
  PORTION, HUNDRED_GRAMS
}
